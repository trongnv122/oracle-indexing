-- Check for partitioning enabled
SELECT * FROM v$option WHERE parameter = 'Partitioning';
--| PARAMETER     | VALUE     | CON_ID    |
--| Partitioning  | FALSE     | 0         |
--> Partition was not enabled => purchase for this feature

-- Create table with one partition has defined
CREATE TABLE sales
(
    sale_id     NUMBER,
    sale_date   DATE,
    sale_amount NUMBER
)
    PARTITION BY RANGE (sale_date)
    INTERVAL (NUMTOYMINTERVAL(1, 'MONTH'))
(
    PARTITION sales_jan2022 VALUES LESS THAN (TO_DATE('01-FEB-2022', 'DD-MON-YYYY'))
);


-- Check for existing partitions
SELECT partition_name, partition_position, high_value
FROM user_tab_partitions
WHERE table_name = 'SALES';

-- PARTITION_NAME  PARTITION_POSITION   HIGH_VALUE
-- --------------  ------------------   ------------------------
-- SALES_JAN2022             1             TO_DATE('01-FEB-2022', 'DD-MON-YYYY')
-- SALES_FEB2022             2             TO_DATE('01-MAR-2022', 'DD-MON-YYYY')
-- SALES_MAR2022             3             TO_DATE('01-APR-2022', 'DD-MON-YYYY')


-- Create partition for existing table:
ALTER TABLE sales ADD PARTITION sales_feb2022
    VALUES LESS THAN (TO_DATE('01-MAR-2022', 'DD-MON-YYYY'));

-- Select data from specific partitions:
SELECT * FROM sales PARTITION (sales_jan2022, sales_feb2022)
WHERE sale_date BETWEEN '01-JAN-2022' AND '28-FEB-2022';
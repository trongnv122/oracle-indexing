-- Check Locking for release database from locks and speedup SQL engine from locking constrain

select  username, machine, status,server, count (*)
from v$session
where status = 'ACTIVE'
group by username, machine, status, server
order by count (*) desc;

-- session active >80 (base on ability of system) we should restart SQL engine

select   username, machine, status,server, count (*)
from v$session
where status = 'INACTIVE'
group by username, machine, status, server
order by count (*) desc;

-- session inactive >250 restart SQL


-- Verify locking sessions (IMPORTANT)
select sid,SERIAL#,username, machine, server, sql_address
from v$session
where blocking_session is not null;

-- Verify locking tables (IMPORTANT)
select a.session_id, a.oracle_username, a.os_user_name, a.process, b.owner,  b.object_name, b.subobject_name, b.object_type
from v$locked_object a, all_objects b
where a.object_id = b.object_id and locked_mode = 3
order by oracle_username, session_id;


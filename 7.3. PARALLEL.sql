-- WITHOUT PARALLEL
EXPLAIN PLAN FOR
select
    a.id, a.person_id, a.street_name, a.street_number, w.CODE
FROM ADDRESSES a, WARDS W
WHERE a.STREET_NAME = W.NAME;

SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));
--0 rows retrieved in 10 s 378 ms (execution: 10 s 367 ms, fetching: 11 ms)
--0 rows retrieved in 6 s 924 ms (execution: 6 s 908 ms, fetching: 16 ms)
--0 rows retrieved in 4 s 438 ms (execution: 4 s 422 ms, fetching: 16 ms)


-- PARALLEL
EXPLAIN PLAN FOR
select /*+ parallel(8) */ a.id,
    a.person_id, a.street_name, a.street_number, w.CODE
FROM  ADDRESSES  a, WARDS W
WHERE a.STREET_NAME = W.NAME;

SELECT * FROM table(DBMS_XPLAN.DISPLAY);
--0 rows retrieved in 4 s 650 ms (execution: 4 s 638 ms, fetching: 12 ms)
--0 rows retrieved in 5 s 919 ms (execution: 5 s 909 ms, fetching: 10 ms)
--0 rows retrieved in 4 s 585 ms (execution: 4 s 577 ms, fetching: 8 ms)




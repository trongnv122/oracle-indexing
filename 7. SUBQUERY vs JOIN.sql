EXPLAIN PLAN FOR
    select a.id, a.person_id, a.street_name, a.street_number,
    (SELECT d.NAME FROM DISTRICTS d WHERE d.CODE = a.DISTRICT_CODE) AS district_name,
    (SELECT w.NAME FROM WARDS W WHERE W.CODE = a.WARD_CODE) AS ward_name,
    (SELECT p.NAME FROM PROVINCES p WHERE p.CODE = a.PROVINCE_CODE) AS province_name
FROM ADDRESSES a
ORDER BY a.ID DESC
OFFSET 0 ROWS FETCH NEXT 500000 ROWS ONLY ;

SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));
--500,000 rows retrieved starting from 1 in 8 s 210 ms (execution: 6 ms, fetching: 8 s 204 ms)
--500,000 rows retrieved starting from 1 in 4 s 19 ms (execution: 86 ms, fetching: 3 s 933 ms)
--500,000 rows retrieved starting from 1 in 3 s 833 ms (execution: 77 ms, fetching: 3 s 756 ms)



EXPLAIN PLAN FOR
SELECT
    a.id, a.person_id, a.street_name, a.street_number,
       w.NAME,
       d.NAME,
       p.NAME,
       ROWNUM
FROM ADDRESSES a
         LEFT JOIN WARDS W ON W.CODE = a.WARD_CODE
         LEFT JOIN DISTRICTS d ON d.CODE = a.DISTRICT_CODE
         LEFT JOIN PROVINCES p ON p.CODE = a.PROVINCE_CODE
ORDER BY a.ID DESC
OFFSET 0 ROWS FETCH NEXT 500000 ROWS ONLY ;

SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));

-- 500,000 rows retrieved starting from 1 in 59 s 303 ms (execution: 28 s 659 ms, fetching: 30 s 644 ms)
-- 500,000 rows retrieved starting from 1 in 55 s 281 ms (execution: 27 s 21 ms, fetching: 28 s 260 ms)
-- 500,000 rows retrieved starting from 1 in 1 m 1 s 834 ms (execution: 29 s 593 ms, fetching: 32 s 241 ms)


-- Use select subquery first
EXPLAIN PLAN FOR
SELECT sub1.*, w.NAME,
       d.NAME,
       p.NAME FROM (
SELECT a.id, a.person_id, a.street_name, a.street_number, WARD_CODE, DISTRICT_CODE, PROVINCE_CODE FROM ADDRESSES a ORDER BY a.ID DESC
OFFSET 0 ROWS FETCH NEXT 500000 ROWS ONLY) sub1
                  LEFT JOIN WARDS W ON W.CODE = sub1.WARD_CODE
                  LEFT JOIN DISTRICTS d ON d.CODE = sub1.DISTRICT_CODE
                  LEFT JOIN PROVINCES p ON p.CODE = sub1.PROVINCE_CODE;

--500,000 rows retrieved starting from 1 in 4 s 774 ms (execution: 370 ms, fetching: 4 s 404 ms)
--500,000 rows retrieved starting from 1 in 4 s 627 ms (execution: 368 ms, fetching: 4 s 259 ms)
--500,000 rows retrieved starting from 1 in 4 s 816 ms (execution: 356 ms, fetching: 4 s 460 ms)

SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));







EXPLAIN PLAN FOR
with sub1 as (SELECT a.id, a.person_id, a.street_name, a.street_number, WARD_CODE, DISTRICT_CODE, PROVINCE_CODE
                    FROM ADDRESSES a OFFSET 0 ROWS FETCH NEXT 500000 ROWS ONLY)
SELECT sub1.* FROM sub1
    LEFT JOIN WARDS W ON W.CODE = sub1.WARD_CODE
                        LEFT JOIN DISTRICTS d ON d.CODE = sub1.DISTRICT_CODE
                        LEFT JOIN PROVINCES p ON p.CODE = sub1.PROVINCE_CODE;

SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));

-- 500,000 rows retrieved starting from 1 in 3 s 715 ms (execution: 105 ms, fetching: 3 s 610 ms)
-- 500,000 rows retrieved starting from 1 in 3 s 637 ms (execution: 100 ms, fetching: 3 s 537 ms)
-- 500,000 rows retrieved starting from 1 in 3 s 446 ms (execution: 54 ms, fetching: 3 s 392 ms)




-- SUBQUERY execute twice
EXPLAIN PLAN FOR
SELECT name,
       (SELECT MAX(PRICE) FROM PRODUCTS) AS max_price
FROM PRODUCTS
WHERE PRICE = (SELECT MAX(PRICE) FROM PRODUCTS);
--1 row retrieved starting from 1 in 16 ms (execution: 4 ms, fetching: 12 ms)
--1 row retrieved starting from 1 in 12 ms (execution: 4 ms, fetching: 8 ms)
--1 row retrieved starting from 1 in 18 ms (execution: 5 ms, fetching: 13 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));


-- SUBQUERY execute once
EXPLAIN PLAN FOR

WITH m AS (SELECT MAX(PRICE) MAX_PRICE FROM PRODUCTS)
SELECT name, m.MAX_PRICE
FROM PRODUCTS, m
WHERE PRICE = m.MAX_PRICE;
--1 row retrieved starting from 1 in 29 ms (execution: 5 ms, fetching: 24 ms)
--1 row retrieved starting from 1 in 12 ms (execution: 2 ms, fetching: 10 ms)
--1 row retrieved starting from 1 in 13 ms (execution: 4 ms, fetching: 9 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));
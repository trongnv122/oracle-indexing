SELECT O.ID, O.PERSON_ID, O.ORDER_TOTAL_AMOUNT, O.BILLING_ADDRESS_ID, O.SHIPPING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A2 on O.SHIPPING_ADDRESS_ID = A2.ID
         INNER JOIN WARDS W on W.CODE = A2.WARD_CODE
WHERE W.NAME in ('Vĩnh Bình', 'Phước Thạnh')

UNION

SELECT O.ID, O.PERSON_ID, O.ORDER_TOTAL_AMOUNT, O.BILLING_ADDRESS_ID, O.SHIPPING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A2 on O.BILLING_ADDRESS_ID = A2.ID
         INNER JOIN WARDS W on W.CODE = A2.WARD_CODE
WHERE W.NAME in  ('Vĩnh Bình', 'Phước Thạnh')
OFFSET 0 ROWS
    FETCH NEXT 500 ROWS ONLY;

-- ==> 500 rows retrieved starting from 1 in 33 s 927 ms (execution: 33 s 889 ms, fetching: 38 ms)
-- ==> 500 rows retrieved starting from 1 in 31 s 871 ms (execution: 31 s 843 ms, fetching: 28 ms)
-- ==> 500 rows retrieved starting from 1 in 31 s 308 ms (execution: 31 s 285 ms, fetching: 23 ms)

SELECT O.ID, O.PERSON_ID, O.ORDER_TOTAL_AMOUNT, O.BILLING_ADDRESS_ID, O.SHIPPING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A2 on O.SHIPPING_ADDRESS_ID = A2.ID
         INNER JOIN WARDS W on W.CODE = A2.WARD_CODE
WHERE W.NAME in ('Vĩnh Bình', 'Phước Thạnh')

UNION ALL

SELECT O.ID, O.PERSON_ID, O.ORDER_TOTAL_AMOUNT, O.BILLING_ADDRESS_ID, O.SHIPPING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A2 on O.BILLING_ADDRESS_ID = A2.ID
         INNER JOIN WARDS W on W.CODE = A2.WARD_CODE
WHERE W.NAME in  ('Vĩnh Bình', 'Phước Thạnh')
OFFSET 0 ROWS
    FETCH NEXT 500 ROWS ONLY;

-- ==> 500 rows retrieved starting from 1 in 11 s 169 ms (execution: 11 s 138 ms, fetching: 31 ms)
-- ==> 500 rows retrieved starting from 1 in 12 s 85 ms (execution: 12 s 60 ms, fetching: 25 ms)
-- ==> 500 rows retrieved starting from 1 in 12 s 367 ms (execution: 12 s 329 ms, fetching: 38 ms)

SELECT DISTINCT D.CODE FROM ADDRESSES A
INNER JOIN WARDS D on D.CODE = A.WARD_CODE

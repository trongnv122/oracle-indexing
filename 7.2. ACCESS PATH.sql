-- ORACLE AUTO  => TABLE ACCESS BY INDEX ROWID
EXPLAIN PLAN FOR
select  *
from  ORDER_LINES OL WHERE OL.ID  = 11;
--1 row retrieved starting from 1 in 24 ms (execution: 7 ms, fetching: 17 ms)
--1 row retrieved starting from 1 in 30 ms (execution: 5 ms, fetching: 25 ms)
--1 row retrieved starting from 1 in 13 ms (execution: 4 ms, fetching: 9 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));

-- FULL ACCESS
EXPLAIN PLAN FOR
select /*+ FULL(OL) */ *
from  ORDER_LINES OL WHERE OL.ID  = 11;
--1 row retrieved starting from 1 in 1 s 199 ms (execution: 1 s 188 ms, fetching: 11 ms)
--1 row retrieved starting from 1 in 1 s 68 ms (execution: 1 s 58 ms, fetching: 10 ms)
--1 row retrieved starting from 1 in 810 ms (execution: 799 ms, fetching: 11 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));





----------------

-- AUTO => TABLE FULL ACCESS
EXPLAIN PLAN FOR
select OL.ID, ORDER_ID, PRODUCT_ID, QTY
from  ORDER_LINES OL WHERE OL.ORDER_ID  < 9999999;
--500,000 rows retrieved starting from 1 in 3 s 94 ms (execution: 27 ms, fetching: 3 s 67 ms)
--500,000 rows retrieved starting from 1 in 3 s 94 ms (execution: 24 ms, fetching: 3 s 70 ms)
--500,000 rows retrieved starting from 1 in 3 s 158 ms (execution: 30 ms, fetching: 3 s 128 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));

-- TABLE ACCESS BY INDEX ROWID BATCHED
EXPLAIN PLAN FOR
select /*+ INDEX(OL IDX_ORDER_LINES_ORDER_ID) */ OL.ID, ORDER_ID, PRODUCT_ID, QTY
from  ORDER_LINES OL WHERE OL.ORDER_ID  < 9999999;
--500,000 rows retrieved starting from 1 in 3 s 309 ms (execution: 52 ms, fetching: 3 s 257 ms)
--500,000 rows retrieved starting from 1 in 3 s 389 ms (execution: 24 ms, fetching: 3 s 365 ms)
--500,000 rows retrieved starting from 1 in 3 s 160 ms (execution: 27 ms, fetching: 3 s 133 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));


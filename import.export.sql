SQL>grant DATAPUMP_EXP_FULL_DATABASE to DEMOUSER;
SQL>create directory X as '/opt/oracle/oradata/bk';

$mkdir -p /opt/oracle/oradata/bk

$expdp demouser/demouser@ORCLCDB full=Y directory=X dumpfile=full.dmp logfile=fullexp.log


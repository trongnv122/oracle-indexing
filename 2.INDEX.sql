-- https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/

SELECT * FROM persons where last_name = 'OX6TFBK216TYQ8B8JM1W2V3RP2ZL9';
-- execution: 9 s 361 ms, fetching: 46 ms

CREATE INDEX index_name    ON persons(last_name);

SELECT * FROM persons where last_name = 'OX6TFBK216TYQ8B8JM1W2V3RP2ZL9';
-- execution: 11 ms, fetching: 15 ms

-- ##########################################################################
---- INDEX ON MULTIPLE COLUMNS
DROP INDEX index_name;


select count(ID)
from PERSONS;

SELECT * FROM persons where last_name = 'I4LLGC5P1VM8D1IWDNC6V';
-- execution: 8 s 983 ms, fetching: 17 ms
-- 0 rows retrieved in 17 s 748 ms (execution: 17 s 720 ms, fetching: 28 ms)

-- 1 row retrieved starting from 1 in 15 s 856 ms (execution: 15 s 821 ms, fetching: 35 ms)

DROP INDEX "PERSONS_LAST_NAME_index";

create index "PERSONS_LAST_NAME_index"
    on PERSONS (FIRST_NAME, LAST_NAME)
/

| FIRST\_NAME | LAST\_NAME |
| :--- | :--- |
-- | 792IWHKKLNHAUSJIY3S6P85KDVBLZ | 84P2L2OPR9XD3GBV3R5SWBYXUL |
EXPLAIN PLAN FOR
select *
from PERSONS where FIRST_NAME  like '%IW%' ;
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));

-- 1 row retrieved starting from 1 in 30 ms (execution: 8 ms, fetching: 22 ms)

EXPLAIN PLAN FOR
SELECT * FROM persons where last_name = 'I4LLGC5P1VM8D1IWDNC6V';
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));


CREATE INDEX index_name    ON persons(USERNAME, FIRST_NAME, LAST_NAME);

SELECT * FROM persons where last_name = 'OX6TFBK216TYQ8B8JM1W2V3RP2ZL9';
-- execution: 9 s 201 ms, fetching: 14 ms
SELECT * FROM persons where USERNAME = 'AHCR3D9XG9Z5MV23TNC3SFPYMIBW';
-- execution: 22 ms, fetching: 14 ms
SELECT * FROM persons where LAST_NAME = 'QQS8NKOH25DYIYK5R4HO1R5E28XZNNV4ZQOXKT4X8HS6';
-- execution: 8 s 920 ms, fetching: 10 ms
EXPLAIN PLAN FOR SELECT * FROM persons where USERNAME = 'AHCR3D9XG9Z5MV23TNC3SFPYMIBW'
AND FIRST_NAME = 'QQS8NKOH25DYIYK5R4HO1R5E28XZNNV4ZQOXKT4X8HS6';
-- execution: 12 ms, fetching: 15 ms

SELECT
    PLAN_TABLE_OUTPUT
FROM
    TABLE(DBMS_XPLAN.DISPLAY());

-- =>>>>> QUERY COLUMNS WITH CORRECT SEQUENCE


-- -- INSERT PERFORMANCE
-- ---- INDEX ON MULTIPLE COLUMNS
-- DROP INDEX index_name;
-- SELECT MAX(ID) FROM PERSONS;
-- INSERT INTO DEMOUSER.PERSONS (ID, DEPT_ID, USERNAME, FIRST_NAME, LAST_NAME, DOB, GENDER, SALARY)
-- VALUES (10000002, 49999, 'AHCR3D9XG9Z5MV23TNC3SFPYMIBW_01', 'QQS8NKOH25DYIYK5R4HO1R5E28XZNNV4ZQOXKT4X8HS6_01', 'OX6TFBK216TYQ8B8JM1W2V3RP2ZL9_01', DATE '1951-05-24', 'M', 290819);
-- -- 1 row affected in 9 ms
-- INSERT INTO DEMOUSER.PERSONS (ID, DEPT_ID, USERNAME, FIRST_NAME, LAST_NAME, DOB, GENDER, SALARY)
-- VALUES (10000003, 49999, 'AHCR3D9XG9Z5MV23TNC3SFPYMIBW_01', 'QQS8NKOH25DYIYK5R4HO1R5E28XZNNV4ZQOXKT4X8HS6_01', 'OX6TFBK216TYQ8B8JM1W2V3RP2ZL9_01', DATE '1951-05-24', 'M', 290819);
-- -- 1 row affected in 5 ms
--
-- CREATE INDEX index_name ON persons(USERNAME, FIRST_NAME, LAST_NAME);
--
-- INSERT INTO DEMOUSER.PERSONS (ID, DEPT_ID, USERNAME, FIRST_NAME, LAST_NAME, DOB, GENDER, SALARY)
-- VALUES (10000005, 49999, 'AHCR3D9XG9Z5MV23TNC3SFPYMIBW_03', 'QQS8NKOH25DYIYK5R4HO1R5E28XZNNV4ZQOXKT4X8HS6_03', 'OX6TFBK216TYQ8B8JM1W2V3RP2ZL9_03', DATE '1951-05-24', 'M', 290819);
-- -- 1 row affected in 6 ms
-- INSERT INTO DEMOUSER.PERSONS (ID, DEPT_ID, USERNAME, FIRST_NAME, LAST_NAME, DOB, GENDER, SALARY)
-- VALUES (10000006, 49999, 'AHCR3D9XG9Z5MV23TNC3SFPYMIBW_06', 'QQS8NKOH25DYIYK5R4HO1R5E28XZNNV4ZQOXKT4X8HS6_06', 'OX6TFBK216TYQ8B8JM1W2V3RP2ZL9_06', DATE '1951-05-24', 'M', 290819);
-- -- 1 row affected in 5 ms
--

-- #####################################################################################################################
-- QUERY INDEX WITH UPPER CASE (FUNCTION)
CREATE INDEX index_name    ON persons(USERNAME, FIRST_NAME, LAST_NAME);
SELECT * FROM persons where USERNAME = 'AHCR3D9XG9Z5MV23TNC3SFPYMIBW';
-- execution: 11 ms, fetching: 17 ms

SELECT * FROM persons where UPPER(USERNAME) = UPPER('AHCR3D9XG9Z5MV23TNC3SFPYMIBW');
-- execution: 10 s 18 ms, fetching: 13 ms
-- execution: 10 s 537 ms, fetching: 14 ms

CREATE INDEX idx_persons_UPPER ON persons (UPPER(USERNAME));

SELECT * FROM persons where UPPER(USERNAME) = UPPER('AHCR3D9XG9Z5MV23TNC3SFPYMIBW');
-- execution: 71 ms, fetching: 65 ms
-- execution: 3 ms, fetching: 14 ms
DROP INDEX idx_persons_UPPER;


-- QUERY INDEX WITH LIKE
SELECT * FROM persons where USERNAME = 'AHCR3D9%';
-- execution: 10 ms, fetching: 12 ms
-- execution: 4 ms, fetching: 11 ms

-- QUERY INDEX WITH LIKE AND '%' AT THE END WORK THE SAME
SELECT * FROM persons where USERNAME = '%NC3SFPYMIBW';
-- execution: 11 ms, fetching: 12 ms
-- execution: 2 ms, fetching: 11 ms

-- CREATE INDEX WITH EXPRESS
SELECT * FROM persons WHERE PERSONS.SALARY * (PERSONS.DEPT_ID - 1) < 100;
-- execution: 1 s 448 ms, fetching: 8 s 760 ms
-- execution: 1 s 385 ms, fetching: 8 s 142 ms


CREATE INDEX idx_PERSONS_SAL_CALC ON PERSONS (SALARY * (DEPT_ID - 1), SALARY, DEPT_ID);
SELECT * FROM persons WHERE PERSONS.SALARY * (PERSONS.DEPT_ID - 1) < 100;
-- execution: 80 ms, fetching: 94 ms
-- execution: 3 ms, fetching: 28 ms


CREATE INDEX idx_ADDRESSES_STREET_NAME ON ADDRESSES (STREET_NAME);
SELECT  e.STREET_NAME FROM ADDRESSES e WHERE e.STREET_NAME = 'BILLING__92TA08DYTKP9AE1I1HXYI46BJP' OFFSET 0 ROWS FETCH NEXT 100 ROWS ONLY;
-- 1 row retrieved starting from 1 in 119 ms (execution: 7 ms, fetching: 112 ms)
-- 1 row retrieved starting from 1 in 24 ms (execution: 3 ms, fetching: 21 ms)
-- 1 row retrieved starting from 1 in 18 ms (execution: 3 ms, fetching: 15 ms)



SELECT  e.STREET_NAME FROM ADDRESSES e WHERE UPPER(e.STREET_NAME) = UPPER('BILLING__92TA08DYTKP9AE1I1HXYI46BJP') OFFSET 0 ROWS FETCH NEXT 100 ROWS ONLY;
-- 1 row retrieved starting from 1 in 17 s 955 ms (execution: 17 s 925 ms, fetching: 30 ms)
-- 1 row retrieved starting from 1 in 11 s 282 ms (execution: 11 s 256 ms, fetching: 26 ms)
-- 1 row retrieved starting from 1 in 12 s 95 ms (execution: 12 s 82 ms, fetching: 13 ms)


CREATE INDEX idx_ADDRESSES_UPPER_STREET_NAME ON ADDRESSES (UPPER(STREET_NAME));
SELECT  e.STREET_NAME FROM ADDRESSES e WHERE UPPER(e.STREET_NAME) = UPPER('BILLING__92TA08DYTKP9AE1I1HXYI46BJP') OFFSET 0 ROWS FETCH NEXT 100 ROWS ONLY;
-- 1 row retrieved starting from 1 in 36 ms (execution: 10 ms, fetching: 26 ms)
-- 1 row retrieved starting from 1 in 23 ms (execution: 4 ms, fetching: 19 ms)
-- 1 row retrieved starting from 1 in 25 ms (execution: 6 ms, fetching: 19 ms)
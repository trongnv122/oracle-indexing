EXPLAIN PLAN FOR
    select a.id, a.person_id, a.street_name, a.street_number,
    (SELECT d.NAME FROM DISTRICTS d WHERE d.CODE = a.DISTRICT_CODE) AS district_name,
    (SELECT w.NAME FROM WARDS W WHERE W.CODE = a.WARD_CODE) AS ward_name,
    (SELECT p.NAME FROM PROVINCES p WHERE p.CODE = a.PROVINCE_CODE) AS province_name
FROM ADDRESSES a
ORDER BY a.ID DESC
OFFSET 0 ROWS FETCH NEXT 500000 ROWS ONLY ;

SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));
--500,000 rows retrieved starting from 1 in 8 s 210 ms (execution: 6 ms, fetching: 8 s 204 ms)
--500,000 rows retrieved starting from 1 in 4 s 19 ms (execution: 86 ms, fetching: 3 s 933 ms)
--500,000 rows retrieved starting from 1 in 3 s 833 ms (execution: 77 ms, fetching: 3 s 756 ms)






--- COMPARE WHERE VS ON in JOIN QUERY

create index "IDX_PRODUCTS_NAME"
    on PRODUCTS (NAME)
/

EXPLAIN PLAN FOR
select O.ID, OL.QTY
from ORDERS O
INNER JOIN ORDER_LINES OL on O.ID = OL.ORDER_ID
where OL.QTY < 5  ;
--402,506 rows retrieved starting from 1 in 2 s 655 ms (execution: 1 s, fetching: 1 s 655 ms)
--402,506 rows retrieved starting from 1 in 2 s 666 ms (execution: 1 s 103 ms, fetching: 1 s 563 ms)
--402,506 rows retrieved starting from 1 in 2 s 738 ms (execution: 1 s 176 ms, fetching: 1 s 562 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));




EXPLAIN PLAN FOR
select O.ID, OL.QTY
from ORDERS O
         INNER JOIN ORDER_LINES OL on O.ID = OL.ORDER_ID AND  OL.QTY < 5;
--402,506 rows retrieved starting from 1 in 2 s 768 ms (execution: 1 s 135 ms, fetching: 1 s 633 ms)
--402,506 rows retrieved starting from 1 in 2 s 818 ms (execution: 1 s 165 ms, fetching: 1 s 653 ms)
--402,506 rows retrieved starting from 1 in 3 s 8 ms (execution: 1 s 259 ms, fetching: 1 s 749 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));



------------------------------------------------------------

-- • USE_HASH / NO_USE_HASH
-- • USE_MERGE / NO_USE_MERGE
-- • USE_NL / NO_USE_NL

/*+ USE_NL */
EXPLAIN PLAN FOR
select /*+ USE_NL(O OL) */ *
--     O.ID, OL.QTY
from  ORDERS O
          INNER JOIN ORDER_LINES OL on O.ID = OL.ORDER_ID AND  OL.QTY < 5;
--402,506 rows retrieved starting from 1 in 7 s 160 ms (execution: 182 ms, fetching: 6 s 978 ms)
--402,506 rows retrieved starting from 1 in 7 s 409 ms (execution: 170 ms, fetching: 7 s 239 ms)
--402,506 rows retrieved starting from 1 in 7 s 196 ms (execution: 183 ms, fetching: 7 s 13 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));


/*+ use_hash */
EXPLAIN PLAN FOR
select /*+ USE_HASH(O OL) */ O.ID, OL.QTY
from  ORDERS O
         INNER JOIN ORDER_LINES OL on O.ID = OL.ORDER_ID AND  OL.QTY < 5;
--402,506 rows retrieved starting from 1 in 2 s 849 ms (execution: 1 s 100 ms, fetching: 1 s 749 ms)
--402,506 rows retrieved starting from 1 in 2 s 767 ms (execution: 1 s 102 ms, fetching: 1 s 665 ms)
--402,506 rows retrieved starting from 1 in 2 s 762 ms (execution: 1 s 95 ms, fetching: 1 s 667 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));




/*+ USE_MERGE */
EXPLAIN PLAN FOR
select /*+ USE_MERGE(O, OL) */ O.ID, OL.QTY
from  ORDERS O
                               INNER JOIN ORDER_LINES OL on O.ID = OL.ORDER_ID AND  OL.QTY < 5;
--402,506 rows retrieved starting from 1 in 3 s 681 ms (execution: 1 s 279 ms, fetching: 2 s 402 ms)
--402,506 rows retrieved starting from 1 in 3 s 634 ms (execution: 1 s 163 ms, fetching: 2 s 471 ms)
--402,506 rows retrieved starting from 1 in 3 s 627 ms (execution: 1 s 175 ms, fetching: 2 s 452 ms)
SELECT * FROM table(DBMS_XPLAN.DISPLAY('plan_table' , NULL  , 'all' ));

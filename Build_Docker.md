# HOW TO RUN ORACLE WITH DOCKER

- login \: docker login -u trong.nguyen@jmango360.com -p Thongoc14 container-registry.oracle.com
```
git clone --no-checkout https://github.com/oracle/docker-images
cd docker-images
git config core.sparseCheckout true
git sparse-checkout init --cone
git sparse-checkout set OracleDatabase/SingleInstance
Pull data
cd OracleDatabase\SingleInstance\dockerfiles

```

copy installation file into ```OracleDatabase\SingleInstance\dockerfiles``` via link https://www.oracle.com/database/technologies/oracle-database-software-downloads.html

Run install docker image: ```./buildContainerImage.sh -v <oracle-db-version> -s ```

Compose yml to run docker
```
version: '3'

services:
  oracle:
    image: oraclelinux:9
    container_name: oracle
    ports:
      - "1521:1521"
      - "5500:5500"
    volumes:
      - ./oracle_data:/opt/oracle/oradata
    networks:
      - oracle-net


networks:
  oracle-net:
    name: oracle_net
```
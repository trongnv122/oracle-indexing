-- Rebuild INDEXES with Tables have update and delete actions frequently
--> Increase performance

-- Plan:
-- 1.Build new index
-- 2.Select query -> check performance
-- 3.Update all row
-- 4.Select query -> check performance
-- 5.Rebuild index
-- 6.Select query -> check performance

-- 1.Build new index
DROP INDEX IDX_ADDRESSES_STREET_NAME;
DROP INDEX IDX_ADDRESSES_UPPER_STREET_NAME;

create index "IDX_ADDRESSES_STREET_NAME"
    on ADDRESSES (STREET_NAME)
/
-- 2.Select query -> check performance
ALTER SYSTEM FLUSH BUFFER_CACHE;
select STREET_NAME, count(*) from ADDRESSES WHERE STREET_NAME like 'BILLING__AB%' group by STREET_NAME ;
-- 7,971 rows retrieved starting from 1 in 22 s 992 ms (execution: 539 ms, fetching: 22 s 453 ms)
-- 7,971 rows retrieved starting from 1 in 11 s 144 ms (execution: 1 s 996 ms, fetching: 9 s 148 ms)
-- 7,971 rows retrieved starting from 1 in 10 s 391 ms (execution: 1 s 91 ms, fetching: 9 s 300 ms)


-- 3.Update all row
UPDATE ADDRESSES SET STREET_NAME = STREET_NAME || '_' WHERE STREET_NAME like 'BILLING__AB%' ;
-- 7,971 rows affected in 7 s 936 ms

-- 4.Select query -> check performance
ALTER SYSTEM FLUSH BUFFER_CACHE;
select STREET_NAME, count(*) from ADDRESSES WHERE STREET_NAME like 'BILLING__AB%' group by STREET_NAME ;
-- 7,971 rows retrieved starting from 1 in 25 s 26 ms (execution: 2 s 115 ms, fetching: 22 s 911 ms)
-- 7,971 rows retrieved starting from 1 in 16 s 663 ms (execution: 2 s 36 ms, fetching: 14 s 627 ms)
-- 7,971 rows retrieved starting from 1 in 25 s 336 ms (execution: 2 s 286 ms, fetching: 23 s 50 ms)

DROP INDEX "IDX_ADDRESSES_STREET_NAME";
create index "IDX_ADDRESSES_STREET_NAME"
    on ADDRESSES (STREET_NAME)
/

-- 4.Select query -> check performance
ALTER SYSTEM FLUSH BUFFER_CACHE;
select STREET_NAME, count(*) from ADDRESSES WHERE STREET_NAME like 'BILLING__AB%' group by STREET_NAME ;
-- 7,971 rows retrieved starting from 1 in 10 s 562 ms (execution: 3 s 23 ms, fetching: 7 s 539 ms)
-- 7,971 rows retrieved starting from 1 in 818 ms (execution: 229 ms, fetching: 589 ms)
-- 7,971 rows retrieved starting from 1 in 847 ms (execution: 235 ms, fetching: 612 ms)
-- 7,971 rows retrieved starting from 1 in 836 ms (execution: 236 ms, fetching: 600 ms)


ALTER SYSTEM FLUSH BUFFER_CACHE;

EXPLAIN PLAN SET statement_id = 'PLAN_QUERY' FOR

SELECT count( 1)
FROM PERSONS p
WHERE EXISTS(SELECT 1
             FROM ADDRESSES o /* Note 1 */
             WHERE p.id = o.PERSON_ID /* Note 2 */
               AND o.PROVINCE_CODE in ('01', '02', '03', '04', '05'));

-- 1 row retrieved starting from 1 in 8 s 362 ms (execution: 8 s 350 ms, fetching: 12 ms)
-- 1 row retrieved starting from 1 in 1 s 690 ms (execution: 1 s 681 ms, fetching: 9 ms)
-- 20,000 rows retrieved starting from 1 in 459 ms (execution: 310 ms, fetching: 149 ms)


-- UPDATE ADDRESSES SET WARD_CODE = '00001' WHERE ID=10000001;
select OPERATION, COST, OPTIONS, OBJECT_OWNER, OBJECT_NAME, OBJECT_ALIAS, OBJECT_TYPE, OPTIMIZER, DEPTH from my_plan_table
WHERE statement_id = 'PLAN_QUERY'
  AND PLAN_ID = (SELECT MAX(PLAN_ID) FROM my_plan_table)
ORDER BY ID;

ALTER SYSTEM FLUSH BUFFER_CACHE;
SELECT /* EXISTS example */
    p.id,
    p.first_name,
    p.last_name,
    p.salary
FROM PERSONS p
INNER JOIN ADDRESSES A on p.ID = A.PERSON_ID
WHERE A.PROVINCE_CODE in ('01', '02', '03', '04', '05');

-- 20,000 rows retrieved starting from 1 in 471 ms (execution: 305 ms, fetching: 166 ms)
-- 20,000 rows retrieved starting from 1 in 447 ms (execution: 289 ms, fetching: 158 ms)
-- 20,000 rows retrieved starting from 1 in 430 ms (execution: 292 ms, fetching: 138 ms)
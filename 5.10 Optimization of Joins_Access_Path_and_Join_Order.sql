-- Indexing lower than full scan if data > 4%

ALTER SYSTEM FLUSH BUFFER_CACHE;
--- INDEX VS FULL SCAN
-- Using Index
select WARD_CODE, count(*) from ADDRESSES group by WARD_CODE;
-- 10,599 rows retrieved starting from 1 in 7 s 300 ms (execution: 7 s 253 ms, fetching: 47 ms)
-- 10,599 rows retrieved starting from 1 in 7 s 200 ms (execution: 7 s 166 ms, fetching: 34 ms)
-- 10,599 rows retrieved starting from 1 in 7 s 183 ms (execution: 7 s 148 ms, fetching: 35 ms)

ALTER SYSTEM FLUSH BUFFER_CACHE;
select /*+ FULL(a)*/  WARD_CODE,  count(*) from ADDRESSES a group by WARD_CODE;
-- 10,599 rows retrieved starting from 1 in 6 s 700 ms (execution: 6 s 644 ms, fetching: 56 ms)
-- 10,599 rows retrieved starting from 1 in 7 s 96 ms (execution: 7 s 58 ms, fetching: 38 ms)
-- 10,599 rows retrieved starting from 1 in 6 s 132 ms (execution: 6 s 94 ms, fetching: 38 ms)



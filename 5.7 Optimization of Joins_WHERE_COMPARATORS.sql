-- > VS >=

SELECT O.ID, O.PERSON_ID, O.ORDER_TOTAL_AMOUNT, O.BILLING_ADDRESS_ID, O.SHIPPING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A on O.BILLING_ADDRESS_ID = A.ID
         INNER JOIN WARDS W on W.CODE = A.WARD_CODE
WHERE W.CODE >=  27775;

-- ==> 500 rows retrieved starting from 1 in 9 s 33 ms (execution: 9 s 8 ms, fetching: 25 ms)
-- ==> 500 rows retrieved starting from 1 in 9 s 7 ms (execution: 8 s 990 ms, fetching: 17 ms)
-- ==> 500 rows retrieved starting from 1 in 8 s 433 ms (execution: 8 s 412 ms, fetching: 21 ms)


SELECT O.ID, O.PERSON_ID, O.ORDER_TOTAL_AMOUNT, O.BILLING_ADDRESS_ID, O.SHIPPING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A on O.BILLING_ADDRESS_ID = A.ID
         INNER JOIN WARDS W on W.CODE = A.WARD_CODE
WHERE W.CODE >  27774;

-- ==> 500 rows retrieved starting from 1 in 9 s 565 ms (execution: 9 s 543 ms, fetching: 22 ms)
-- ==> 500 rows retrieved starting from 1 in 8 s 470 ms (execution: 8 s 445 ms, fetching: 25 ms)
-- ==> 500 rows retrieved starting from 1 in 7 s 187 ms (execution: 7 s 166 ms, fetching: 21 ms)

-- BETWEEN

SELECT O.ID, O.PERSON_ID, O.ORDER_TOTAL_AMOUNT, O.BILLING_ADDRESS_ID, O.SHIPPING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A on O.BILLING_ADDRESS_ID = A.ID
         INNER JOIN WARDS W on W.CODE = A.WARD_CODE
WHERE W.CODE >=  27775 AND W.CODE <= 28846;

-- ==> 500 rows retrieved starting from 1 in 9 s 359 ms (execution: 9 s 339 ms, fetching: 20 ms)
-- ==> 500 rows retrieved starting from 1 in 10 s 202 ms (execution: 10 s 181 ms, fetching: 21 ms)
-- ==> 500 rows retrieved starting from 1 in 7 s 409 ms (execution: 7 s 391 ms, fetching: 18 ms)

SELECT O.ID, O.PERSON_ID, O.ORDER_TOTAL_AMOUNT, O.BILLING_ADDRESS_ID, O.SHIPPING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A on O.BILLING_ADDRESS_ID = A.ID
         INNER JOIN WARDS W on W.CODE = A.WARD_CODE
WHERE W.DISTRICT_CODE in ('001' ,'009');

-- ==> 0 rows retrieved in 1 m 23 s 200 ms (execution: 1 m 23 s 174 ms, fetching: 26 ms)
-- ==> 500 rows retrieved starting from 1 in 9 s 543 ms (execution: 9 s 525 ms, fetching: 18 ms)
-- ==> 500 rows retrieved starting from 1 in 7 s 586 ms (execution: 7 s 563 ms, fetching: 23 ms)

SELECT DISTINCT O.BILLING_ADDRESS_ID
FROM ORDERS O
         INNER JOIN ADDRESSES A on O.BILLING_ADDRESS_ID = A.ID
         INNER JOIN WARDS W on W.CODE = A.WARD_CODE;

select *
from ADDRESSES OFFSET 0 ROWS FETCH NEXT 500 ROWS ONLY;
